﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Windows.Input;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.VisualStudio.TestTools.UITesting;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Microsoft.VisualStudio.TestTools.UITest.Extension;
using Keyboard = Microsoft.VisualStudio.TestTools.UITesting.Keyboard;


namespace CodedUISystemTestProject1
{
    /// <summary>
    /// Summary description for CodedUITest1
    /// </summary>
    [CodedUITest]
    public class CodedUISystemTest1
    {
        public CodedUISystemTest1()
        {
        }
        [DataSource("Microsoft.VisualStudio.TestTools.DataSource.CSV", "|DataDirectory|\\input.csv", "input#csv", DataAccessMethod.Sequential), DeploymentItem("../../../TestInput/input.csv"), TestMethod]
       // [TestMethod]
        public void CodedUITestMethod1()
        {
            // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
            this.UIMap.HomePage();
            this.UIMap.AddChequeButtonAssertion();
            this.UIMap.ClickAddChequeButton();
            this.UIMap.ChequEDisplayedAssertion();
            this.UIMap.InputDataParams.UIItemEditText = TestContext.DataRow["name"].ToString();
            this.UIMap.InputDataParams.UIItemEdit1Text = TestContext.DataRow["datevalue"].ToString();
            this.UIMap.InputDataParams.UIAmountEditText = TestContext.DataRow["amount"].ToString();
            this.UIMap.InputData();
            this.UIMap.ClickAddCheque2();
            this.UIMap.ValueInTextAssertionExpectedValues.UIThesumofOnehundredanPaneDisplayText = TestContext.DataRow["expectedresult"].ToString();
            this.UIMap.ValueInTextAssertion();
            this.UIMap.CloseWindow();
        }

        #region Additional test attributes

        // You can use the following additional attributes as you write your tests:

        ////Use TestInitialize to run code before running each test 
        //[TestInitialize()]
        //public void MyTestInitialize()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        ////Use TestCleanup to run code after each test has run
        //[TestCleanup()]
        //public void MyTestCleanup()
        //{        
        //    // To generate code for this test, select "Generate Code for Coded UI Test" from the shortcut menu and select one of the menu items.
        //}

        #endregion

        /// <summary>
        ///Gets or sets the test context which provides
        ///information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get
            {
                return testContextInstance;
            }
            set
            {
                testContextInstance = value;
            }
        }
        private TestContext testContextInstance;

        public UIMap UIMap
        {
            get
            {
                if ((this.map == null))
                {
                    this.map = new UIMap();
                }

                return this.map;
            }
        }

        private UIMap map;
    }
}
