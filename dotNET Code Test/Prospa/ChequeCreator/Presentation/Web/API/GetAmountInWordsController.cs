﻿using System.Web.Http;
using Prospa.ChequeCreator.Services.Interfaces.ICurrencyTranslatorOrchestration;

namespace Prospa.ChequeCreator.Presentation.Web.API
{
    public class GetAmountInWordsController : ApiController
    {
        private readonly ICurrencyTranslatorOrchestration _currencyTranslatorOrchestration;

        public GetAmountInWordsController(ICurrencyTranslatorOrchestration currencyTranslatorOrchestration)
        {
            _currencyTranslatorOrchestration = currencyTranslatorOrchestration;
        }

        [Route("API/GetAmountInWords", Name =  "GetAmountInWords")]
        public string Post([FromBody]decimal amount)
        {
            return _currencyTranslatorOrchestration.GetAmountInWords(amount);
        }

    }
}