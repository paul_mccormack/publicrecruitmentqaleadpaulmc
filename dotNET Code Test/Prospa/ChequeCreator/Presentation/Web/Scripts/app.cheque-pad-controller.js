﻿(function() {
    var app = angular.module('cheque-pad', []);

    app.controller('ChequePadController', [
        '$scope', function($scope) {
            $scope.cheques = [];

            $scope.addCheque = function() {
                $scope.cheques.push({
                    
                });
            }

            $scope.blankSlate = function () {
                return $scope.cheques.length === 0;
            }

        }
    ]);

    app.controller('ChequeController', [
        '$scope', '$filter', 'getAmountInWords', function ($scope, $filter, getAmountInWords) {
            $scope.humanize = function () {
                $scope.amount = Number($filter('number')($scope.amount, 2));
                getAmountInWords($scope.amount, function(sumof) {
                    $scope.sumof = sumof;
                });
            }
        }
    ]);

    app.factory('getAmountInWords', [
        '$http', 'getAmountInWordsEndpoint', function ($http, getAmountInWordsEndpoint) {
            return function(amount, completed) {
                var data = amount;
                $http.post(getAmountInWordsEndpoint, data).then(function (r) {
                    console.log(r);
                    completed(r.data);
                });
            }
        }
    ]);

})(angular);